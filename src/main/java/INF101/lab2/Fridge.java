package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    final int MAXSIZE = 20;
    ArrayList<FridgeItem> itemsInFridge;

    public Fridge() {
        this.itemsInFridge = new ArrayList<FridgeItem>();
    }


    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return MAXSIZE;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() >= MAXSIZE) {
            return false;
        } else {
            itemsInFridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsInFridge.contains(item)) {
            itemsInFridge.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        itemsInFridge = new ArrayList<FridgeItem>();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem expired : expiredItems) {
            itemsInFridge.remove(expired);
        }
        return expiredItems;
    }
}
